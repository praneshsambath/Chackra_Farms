import { Component } from "@angular/core";
import { NavController } from "ionic-angular";
import { AppDatewiseReport } from "../datewise/datewise";
import { AppCustomerwiseReport } from "../customerwise/customerwise";


@Component({
    selector: 'App-Dashboard',
    templateUrl: 'dash.html'

})

export class AppDashboard {

    details = [
        { date: '27-03-2018', trip: 'M', prod_content: '100L', delivered_content: '100L', total_tokken: '85', collected_tokken: '80', tot_bottle: '100', collected_bottle: '80' },
        { date: '28-03-2018', trip: 'M', prod_content: '100L', delivered_content: '65L', total_tokken: '80', collected_tokken: '80', tot_bottle: '100', collected_bottle: '60' },
        { date: '29-03-2018', trip: 'M', prod_content: '100L', delivered_content: '100L', total_tokken: '65', collected_tokken: '60', tot_bottle: '100', collected_bottle: '80' },
        { date: '30-03-2018', trip: 'M', prod_content: '100L', delivered_content: '90L', total_tokken: '75', collected_tokken: '70', tot_bottle: '100', collected_bottle: '65' },
        { date: '31-03-2018', trip: 'M', prod_content: '100L', delivered_content: '86L', total_tokken: '85', collected_tokken: '60', tot_bottle: '100', collected_bottle: '75' },
        { date: '01-04-2018', trip: 'M', prod_content: '100L', delivered_content: '50L', total_tokken: '100', collected_tokken: '80', tot_bottle: '100', collected_bottle: '90' },
        { date: '02-04-2018', trip: 'M', prod_content: '100L', delivered_content: '60L', total_tokken: '85', collected_tokken: '80', tot_bottle: '100', collected_bottle: '100' },
        { date: '03-04-2018', trip: 'M', prod_content: '100L', delivered_content: '100L', total_tokken: '80', collected_tokken: '80', tot_bottle: '100', collected_bottle: '54' },
    ]
}