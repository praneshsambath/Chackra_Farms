import { Component, ViewChild, ElementRef, NgZone } from "@angular/core";
import { NavController, PopoverController } from "ionic-angular";
import { flatten } from "@angular/compiler";
import { AppPopover } from "../popover/popover";
// import { MapsAPILoader } from '@agm/core';
// import { } from '@types/googlemaps';


@Component({
    selector: 'delivery-details',
    templateUrl: 'delivery.html'
})

export class AppDeliveryDetails {

    // @ViewChild('search') public searchElement: ElementRef

    individual_details;

    stat: boolean; icon;

    status = false;
    content_details = [
        { product: 'Milk', content: ['1/2 kg', '1kg', '2kg', '3.5kg'], quantity: '2', },
        { product: 'Ghee', content: ['1/2 kg', '1kg', '2kg', '3.5kg'], quantity: '4' }
    ];

    constructor(private navctrl: NavController, public popoverCtrl: PopoverController) {

        // this.individual_details = JSON.parse(localStorage.getItem('individual_details'));

        // console.log(this.individual_details);
    }

    ngOnInit() {

        // this.mapsAPILoader.load().then(
        //     () => {
        //         let autocomplete = new google.maps.places.Autocomplete(this.searchElement.nativeElement, { types: ["address"] });
        //         autocomplete.addListener("place_changed", () => {

        //             this.ngZone.run(() => {
        //                 let place: google.maps.places.PlaceResult = autocomplete.getPlace();

        //                 if (place.geometry === undefined || place.geometry === null) {
        //                     return
        //                 }
        //             })

        //         });
        //     }
        // )
    }

    onLongPress() {
        // this.stat==false?true:false
    }

    presentPopover(myEvent) {
        let popover = this.popoverCtrl.create(AppPopover);
        popover.present({
            ev: myEvent
        });
    }

    OnClick() {
        if (this.status == false) {
            this.status = true;
            this.icon = 'ios-remove-circle-outline';
        }
        else if (this.status == true) {
            this.status = false
            this.icon = 'ios-add-circle-outline';
        }
    }
    saveItem(value) {
        console.log(value);
    }
}
