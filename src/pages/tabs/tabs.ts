import { Component } from '@angular/core';

import { AboutPage } from '../about/about';
import { HomePage } from '../home/home';
import { AppDashboard } from '../dashboard/dashboard_details/dash';
import { AppProductReturnStatus } from '../product_return/product';
import { AppAddCustomer } from '../Add_Customer/add_customer';
import { AppSettings } from '../settings/settings';
import { AppTrip_on_Off } from '../trip_on_off/trip';


@Component({
  selector:'tab-page',
  templateUrl: 'tabs.html'
})
export class TabsPage {

  constructor() {

  }
  // tab0root = AppDashboard;
  tab1Root = HomePage;
  tab2Root = AppAddCustomer;
  tab3Root = AppProductReturnStatus;
  tab4Root = AppDashboard;
  tab5Root = AppSettings;


}
