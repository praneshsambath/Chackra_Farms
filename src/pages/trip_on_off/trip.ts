import { Component } from "@angular/core";
import { TabsPage } from "../tabs/tabs";
import { NavController } from "ionic-angular";

@Component({

    selector: 'trip_on_off-page',
    templateUrl: 'trip.html',

})

export class AppTrip_on_Off {

    visible: boolean;
    milk: boolean;
    prod:boolean;
    constructor(private navctrl:NavController){}

    show_hide_trip_alert() {
        this.visible = true;
    }

    save()
    {
        this.navctrl.push(TabsPage)
    }

    Add_product()
    {
        this.prod = true;
    }
}