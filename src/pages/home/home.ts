import { Component, ViewChild, Renderer } from '@angular/core';
import { NavController, PopoverController, ViewController } from 'ionic-angular';
import { AppPopover } from '../popover/popover';
import { ModalController } from 'ionic-angular';
import { AppAccordian } from '../accordian/accordian';
import { AppmodalPage } from '../ModalPage/modal';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  @ViewChild('attr.id') accordian: any;
  status: boolean = false; visible; showdetails: boolean = false;
  showmap: boolean = false; icon; res;
  slider = true; del1 = false;
  trip: boolean; del = false;show_tokken_slide;
  driver = [
    { mobile: "9638527410", img: 'assets/imgs/milk_half.png', img1: 'assets/imgs/milk_full.png', other: 'more', name: 'Kannan', id: '1001', status: 'delivered', visible: false, showmap: false, lat: 51.678418, lng: 7.809007, expect_delTime: '05:10', address: '101,2nd cross-street,Usha Theatre', price1: "100", price2: "Rs.50" },
    { mobile: "9876543210", img: 'assets/imgs/milk_full.png', other: 'more', name: 'Guru', id: '1002', status: 'Not Delivered', visible: false, showmap: false, lat: 51.678418, lng: 7.809007, expect_delTime: '05:20', address: '155A,RSP Street,Ambedgar Statue', price1: "Rs.100", price2: "50" },
    { mobile: "9621457896", img: 'assets/imgs/milk_half.png', other: 'more', name: 'Sridhar', id: '1003', status: 'not', visible: false, showmap: false, lat: 51.678418, lng: 7.809007, expect_delTime: '05:35', address: ',101A pkn building,Ishwarya Street', price1: "Rs.50", price2: "Rs.25" },
    { mobile: "9632587410", img: 'assets/imgs/milk_full.png', name: 'Aravind', id: '1004', other: 'more', status: 'Not Delivered', visible: false, showmap: false, lat: 51.678418, lng: 7.809007, expect_delTime: '05:55', address: '10A,1st cross street,Venkateshwara illam', price1: "Rs.75", price2: "Rs.30" },
    { mobile: "9876543210", img: 'assets/imgs/milk_full.png', img1: 'assets/imgs/milk_full.png', name: 'Sudhakar', id: '1005', other: 'more', status: 'not', visible: false, showmap: false, lat: 51.678418, lng: 7.809007, expect_delTime: '06:15', address: '141N,Sree Illam,Ayyanpalayam,', price1: "Rs.100", price2: "Rs.20" },
    { mobile: "9517534562", img: 'assets/imgs/milk_half.png', other: 'more', name: 'Gowtham', id: '1006', status: 'delivered', visible: false, showmap: false, lat: 51.678418, lng: 7.809007, expect_delTime: '06:30', address: '102A,RSR Building,AmaravathiPalayam', price1: "Rs.100", price2: "Rs.50" },
    { mobile: "9637418526", img: 'assets/imgs/milk_full.png', img1: 'assets/imgs/milk_full.png', other: 'more', name: 'Mohan', id: '1007', status: 'delivered', visible: false, showmap: false, lat: 51.678418, lng: 7.809007, expect_delTime: '06:35', address: '100A,1st cross street,Kattupalayam', price1: "Rs.100", price2: "Rs.50" },
    { mobile: "9876543214", img: 'assets/imgs/milk_half.png', name: 'Sujay', id: '1008', other: 'more', status: 'On Process', visible: false, showmap: false, lat: 51.678418, lng: 7.809007, expect_delTime: '06:50', address: '110A,viveka illam,Palladam Road,', price1: "Rs.100", price2: "Rs.750" },
    { mobile: "9517531235", img: 'assets/imgs/milk_full.png', img1: 'assets/imgs/milk_full.png', other: 'more', name: 'Krishva', id: '1009', status: 'delivered', visible: false, showmap: false, lat: 51.678418, lng: 7.809007, expect_delTime: '07:10', address: '12B,Rsun appartment,Kattupalayam ', price1: "Rs.100", price2: "Rs.50" }
  ]
  constructor(public modalCtrl: ModalController, public render: Renderer, public navCtrl: NavController, private popoverCtrl: PopoverController, private view: ViewController) {

    // this.presentModal();
  }
  makeCall(value, call) {
    value = encodeURIComponent(value);
    window.location.href = "tel:" + value;
  }
  toggle_button(value) {
    console.log(value);
    this.show_tokken_slide=value;
  }
  cash_received(value) {
    console.log(value);
  }

  // Onclick(value, i) {
  //   this.render.setElementStyle(this.accordian.nativeElement, "webkitTransition", "max-height 500ms,padding 500ms");
  //   if (this.status) {
  //     this.render.setElementStyle(this.accordian.nativeElement, "max-height", "0px");
  //     this.render.setElementStyle(this.accordian.nativeElement, "padding", "0px 16px");
  //   }

  //   else {
  //     this.render.setElementStyle(this.accordian.nativeElement, "max-height", "500px");
  //     this.render.setElementStyle(this.accordian.nativeElement, "padding", "13px 16px");

  //   }
  //   this.status = !this.status;
  // }
  delivery1() {
    if (this.del1 == false) {
      this.del1 = true;
    }
    else if (this.del1 == true) {
      this.del1 = false;
    }
  }
  delivery() {
    if (this.del == false) {
      this.del = true;
    }
    else if (this.del == true) {
      this.del = false;
    }
  }

  closeMap(value, value1) {
    console.log(value, "KPK", value1)
    // this.status = false;
    for (let i = 0; i < this.driver.length; i++) {
      this.driver[i].showmap = false;
    }
  }

  Onclick(value, i) {
    console.log(value);
    console.log(i);

    for (let i = 0; i < this.driver.length; i++) {
      if (value.id == this.driver[i].id) {
        if (this.driver[i].visible) {
          this.driver[i].visible = false;
          this.slider = true;
        }
        else {
          this.driver[i].visible = true;
          this.slider = false;
        }
      }
      else {
        this.status = false;
        this.driver[i].visible = false;
      }
    }
  }

  // closeMap(map,map1) {
  //   this.showMap(map,map1)
  // }
  // Onclick(value, i) {
  //   this.render.setElementStyle(this.accordian.nativeElement, "webkitTransition", "max-height 500ms,padding 500ms");
  //   for (let details of this.driver) {
  //     if (value.id == details.id) {
  //       if (this.status) {
  //         this.render.setElementStyle(this.accordian.nativeElement, "max-height", "0px");
  //         this.render.setElementStyle(this.accordian.nativeElement, "padding", "0px 16px");
  //       }

  //       else {

  //         this.res = details.id;
  //         thi  s.visible = true;
  //         this.render.setElementStyle(this.accordian.nativeElement, "max-height", "500px");
  //         this.render.setElementStyle(this.accordian.nativeElement, "padding", "13px 16px");

  //       }

  //       this.status = !this.status;
  //     }
  //   }
  // this.render.setElementStyle(this.accordian.nativeElement, "webkitTransition", "max-height 500ms,padding 500ms");
  // if (this.status) {
  //   this.render.setElementStyle(this.accordian.nativeElement, "max-height", "0px");
  //   this.render.setElementStyle(this.accordian.nativeElement, "padding", "0px 16px");
  // }

  // else {
  //   // this.visible = true

  //   this.render.setElementStyle(this.accordian.nativeElement, "max-height", "500px");
  //   this.render.setElementStyle(this.accordian.nativeElement, "padding", "13px 16px");

  // }

  // this.status = !this.status;
  // }

  // Onclick(value, index) {
  // console.log(value.visible);
  //   for (let det of this.driver) {
  //     if (det.id == value.id) {
  //       det.visible = true;
  //     }
  //   }

  // console.log(this.driver);
  // }

  // contactDetails(value) {
  //   localStorage.setItem('individual_details', JSON.stringify(value));
  //   this.navCtrl.push(AppDeiveryDetails);
  // }
  // reorderItems(value) {
  //   console.log(value)
  // }
  presentPopover(value) {
    console.log(value);
    let popover = this.popoverCtrl.create(AppPopover);
    popover.present({
      ev: value
    });
    popover.onDidDismiss(popoverData => {
      console.log(popoverData);
    })
  }


  showMap(value, value1) {

    value1.close();
    for (let i = 0; i < this.driver.length; i++) {
      if (value.id == this.driver[i].id) {
        if (this.driver[i].showmap || this.driver[i].visible) {
          this.driver[i].showmap = false;
          this.driver[i].visible = false;
        }
        else {
          this.driver[i].showmap = true;
          this.driver[i].visible = false;
        }
      }
      else {
        this.driver[i].showmap = false;
        this.driver[i].visible = false;
      }
    }

  }

  // expand(value) {
  //   console.log(value);

  //   for (let det of this.driver) {

  //     if (det.id == value.id) {
  //       this.visible = det.id;
  //     }

  //   }
  //   if (this.status == false) {
  //     this.status = true;
  //     // this.showMap();
  //   }
  //   else if (this.status == true) {
  //     this.status = false;
  //     // this.showMap();
  //   }
  // }
  // showDetails() {
  //   if (!this.showdetails) {
  //     this.showdetails = true;
  //   }
  //   else {
  //     this.showdetails = false;
  //   }
  // }


}



