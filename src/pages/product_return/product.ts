import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

@Component({
  selector: 'product-return',
  templateUrl: 'product.html'
})
export class AppProductReturnStatus {

  constructor(public navCtrl: NavController) {

  }

}
