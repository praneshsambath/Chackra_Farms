import { Component } from "@angular/core";
import { AlertController, NavController } from "ionic-angular";
import { AppLogin } from "../Login/login";

@Component({
    selector: 'App-Settings',
    templateUrl: 'settings.html'
})

export class AppSettings {
    session;
    sess = "0";
    constructor(private alertCtrl: AlertController, private navCtrl: NavController) { }


    End_trip() {
        let alert = this.alertCtrl.create({
            title: 'Log Out',
            message: 'Are you Sure to End the Trip ',
            buttons: [
                {
                    text: 'Yes',
                    handler: () => {

                        this.ending_trip_alert();

                    }
                },
                {
                    text: 'NO',
                    handler: () => {

                    }
                }
            ]
        });

        alert.present();
    }
    ending_trip_alert() {
        this.session = localStorage.getItem("session")

        if (this.session == "1") {
            localStorage.setItem("session", this.sess)
            this.navCtrl.push(AppLogin)
        }

    }
}