import { Component } from "@angular/core";
import { NavController, AlertController } from "ionic-angular";
import { TabsPage } from "../tabs/tabs";
import { HomePage } from "../home/home";
import { AppTrip_on_Off } from "../trip_on_off/trip";


@Component({
    selector: 'login-page',
    templateUrl: 'login.html'
})
export class AppLogin {
    sess = "1"; session
    showPsw = true;
    valid: boolean = true;
    canceled_trip = true;
    constructor(private navCtrl: NavController, private alertCtrl: AlertController) { }




    checkUser(Password) {

        if (Password == "1") {
            // this.trip_Alert();
            this.navCtrl.push(AppTrip_on_Off)
        }
        else {
            this.valid = false;
        }
    }


    res() {
        if (this.showPsw == true) {
            this.showPsw = false;
        }
        else if (this.showPsw == false) {
            this.showPsw = true
        }
    }
}