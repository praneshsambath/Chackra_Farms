import { Component } from '@angular/core';
import { Platform, NavController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { TabsPage } from '../pages/tabs/tabs';
import { AppLogin } from '../pages/Login/login';
import { HomePage } from '../pages/home/home';
import { AppTrip_on_Off } from '../pages/trip_on_off/trip';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {

  rootPage: any = AppLogin;

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen) {

    var temp = localStorage.getItem("session")
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.

      statusBar.styleDefault();
      splashScreen.hide();
    });

    // if (temp == "1") {
    //   this.rootPage = TabsPage;
    // }
    // else {
    //   this.rootPage = AppTrip_on_Off;
    // }
  }
}
