import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

import { AboutPage } from '../pages/about/about';

import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { AgmCoreModule } from '@agm/core';
import { AppPopover } from '../pages/popover/popover';
import { AppAccordian } from '../pages/accordian/accordian';
import { AppmodalPage } from '../pages/ModalPage/modal';
import { AppLogin } from '../pages/Login/login';
import { AppDatewiseReport } from '../pages/dashboard/datewise/datewise';
import { AppDashboard } from '../pages/dashboard/dashboard_details/dash';
import { AppCustomerwiseReport } from '../pages/dashboard/customerwise/customerwise';
import { AppDeliveryDetails } from '../pages/deliver_details/delivery';
import { AppProductReturnStatus } from '../pages/product_return/product';
import { AppAddCustomer } from '../pages/Add_Customer/add_customer';
import { AppSettings } from '../pages/settings/settings';
import { AppTrip_on_Off } from '../pages/trip_on_off/trip';
// import { GoogleMaps } from '@ionic-native/google-maps';

@NgModule({
  declarations: [
    AppTrip_on_Off,
    AppDashboard,
    AppDatewiseReport,
    AppCustomerwiseReport,
    MyApp,
    AboutPage,
    AppProductReturnStatus,
    HomePage,
    TabsPage,
    AppPopover,
    AppAccordian,
    AppmodalPage,
    AppLogin,
    AppDeliveryDetails,
    AppAddCustomer,
    AppSettings
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp, { tabsHideOnSubPages: true }),
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyAaAkaJa9tzdAnwzstWDLTf-Ux-cWUmDtM',
      libraries: ["places"]
    })
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    AppTrip_on_Off,
    MyApp,
    AppLogin,
    AppDatewiseReport,
    AppCustomerwiseReport,
    AboutPage,
    AppProductReturnStatus,
    HomePage,
    TabsPage,
    AppDashboard,
    AppPopover,
    AppAccordian,
    AppmodalPage,
    AppDeliveryDetails,
    AppAddCustomer,
    AppSettings

  ],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: ErrorHandler, useClass: IonicErrorHandler }
  ]
})
export class AppModule { }
